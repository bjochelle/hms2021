<?php include 'header.php';?>
	
	<!-- Start All Pages -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Rooms <?php echo $_SESSION['msg'];?></h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->
	
	<!-- Start Menu -->
	<div class="menu-box">
		<div class="container">
		
				
			<div class="row special-list">
                <?php
                $result = $connectDB->query("SELECT * FROM category as c, rooms as r where c.category=r.category" );

                $count = 1;
                if ($result) {
                while($row = mysqli_fetch_array($result))
                {
                    $category = $row['category'];
                    ?>

                    <div class="col-lg-12 special-grid drinks">
                        <div class="col-lg-6 gallery-single fix" style="float: left">
                            <img src="admin/img/<?php echo  $row['image'];?>" class="img-fluid" alt="Image" style="object-fit: cover;height: 300px;width: 100%;">
                        </div>
                        <div class="col-lg-4" style="float: left;top: 80px;">
                            <h1><?php echo  $row['description'];?></h1>
                            <h3>Php <?php echo  $row['price'];?></h3><br>
                            <h1>Room number: <?php echo  $row['room_number'];?></h1>
                        </div>
                        <div class="col-lg-2" style="float: left;top:100px;">
                        	<?php  if(empty($_SESSION['id'])){?>
								<a href="login.php"> <button class="btn btn-primary bold" style="background-color: #eaa07c;"> Book now</button>
                        		<?php }else{?>
                           <a href="roomDetails.php?id=<?php echo  $row['room_id'];?>"> <button class="btn btn-primary bold" style="background-color: #eaa07c;"> Book now</button>
                           	<?php }?>
                        </div>
                    </div>

                <?php }}?>

			</div>
		</div>
	</div>
	<!-- End Menu -->
	<!-- Start Contact info -->
	<?php include "footer.php";?>