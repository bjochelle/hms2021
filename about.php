<?php include 'header.php';?>
	<!-- Start header -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>About Us</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End header -->
	
	<!-- Start About -->
	<div class="about-section-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<img src="images/header.jpeg" alt="" class="img-fluid">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 text-center">
					<div class="inner-column">
						<h1>All About<span>Costa Leona Hotel</span></h1>
						<h4>Little Story</h4>
						<p>Costa Leona Hotel is a newest modern hotel. We aim to provide peace of mind, consistency and build loyalty based on our value of our relationships with the clients.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End About -->
<?php include "footer.php";?>