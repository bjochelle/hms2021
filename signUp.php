<?php 

  $msg ='';
 include 'connection.php';
 if(isset($_POST['submit'])) {
 	       $fname = $_POST['fname'];
	    $lname = $_POST['lname'];
	    $dob = $_POST['dob'];
	    $address = $_POST['address'];
	    $contact_number = $_POST['contact_number'];
	    $email = $_POST['email'];
	    $username = $_POST['username'];
	    $password = $_POST['password']; 

		 $result = $connectDB -> query("SELECT * FROM user where username ='$username' " );
            // Perform query
            if ($result->num_rows == 0) {
            	    $sql = "INSERT INTO user (fname, lname, dob,address,contact_number,email,username,password,user_type)
				    VALUES ('$fname','$lname','$dob','$address','$contact_number','$email','$username','$password','C')";

				    if ($connectDB->query($sql)) {
				         $msg =  "New record created successfully. <a href='login.php'> Please click here </a>";
				    } else {
				         $msg = "Error: " . $sql . "<br>" . $connectDB->error;
				    }
            }else{
				$msg = "Username is already Existed";
            }
 }
?>
<?php include 'header.php';?>
	
	<!-- Start All Pages -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Sign Up</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->
	
	<!-- Start Contact -->
	<div class="contact-box">
		<div class="container">
			<div id="msgSubmit" class="h3 text-center" style="color:green;"> <?php  echo $msg ;?></div><br>
			
			<div class="row">
				<div class="col-lg-12">
					<form method="POST">
						<div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="fname" name="fname" placeholder="Your Firstname" required data-error="Please enter your Firstname">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Your Lastname" required data-error="Please enter your Lastname">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="date" class="form-control" id="dob" name="dob" placeholder="Your Birthday" required data-error="Please enter your Birthday">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Your email" required data-error="Please enter your email">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="Your Contact Number" required data-error="Please enter your Contact Number">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea type="date" class="form-control" id="address" name="address" placeholder="Your address" required data-error="Please enter your address"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control" id="name" name="username" placeholder="Your username" required data-error="Please enter your username">
									<div class="help-block with-errors"></div>
								</div>                                 
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<input type="password" placeholder="Your password" id="password" class="form-control" name="password" required data-error="Please enter your password">
									<div class="help-block with-errors"></div>
								</div> 
							</div>
						
							<div class="col-md-12">
								
								<div class="submit-button text-center">
									<button class="btn btn-common" id="submit" name="submit" type="submit"  value="Submit" >Register</button>
									
									<div class="clearfix"></div> 
								</div>
							</div>
							<a href="login.php">Already have an acoount? Sign in here</a>
						</div>            
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- End Contact -->
	
	<!-- Start Contact info -->
	<?php include "footer.php";?>