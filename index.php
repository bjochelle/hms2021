<?php include 'header.php';?>
	<!-- Start slides -->
	<div id="slides" class="cover-slides">
		<ul class="slides-container">
			<li class="text-center">
				<img src="images/header.jpeg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="m-b-20"><strong>Welcome To <br> Costa Leona Hotel</strong></h1>
						</div>
					</div>
				</div>
			</li>
			<li class="text-center">
				<img src="images/header2.jpg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="m-b-20"><strong>Welcome To <br> Costa Leona Hotel</strong></h1>
						</div>
					</div>
				</div>
			</li>
			<li class="text-center">
				<img src="images/header4.jpg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="m-b-20"><strong>Welcome To <br> Costa Leona Hotel</strong></h1>
						</div>
					</div>
				</div>
			</li>
		</ul>
		<div class="slides-navigation">
			<a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
			<a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
		</div>
	</div>
	<!-- End slides -->
	
	<!-- Start About -->
	<div class="about-section-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<img src="images/header.jpeg" alt="" class="img-fluid">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 text-center">
					<div class="inner-column">
						<h1>All About <span>Costa Leona Hotel</span></h1>
						<h4>Little Story</h4>
						<p>Costa Leona Hotel is a newest modern hotel. We aim to provide peace of mind, consistency and build loyalty based on our value of our relationships with the clients.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End About -->
	
	<!-- Start QT -->
	<div class="qt-box qt-background">
		<div class="container">
			<div class="row">
				<div class="col-md-8 ml-auto mr-auto text-left">
					<p class="lead ">
						"A vacation is what you take when you can no longer take what you’ve been taking. "
					</p>
					<span class="lead">Earl Wilson</span>
				</div>
			</div>
		</div>
	</div>
	<!-- End QT -->

	
	<!-- Start Gallery -->
	<div class="gallery-box">
		<div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="heading-title text-center">
                        <h2>Rooms</h2>
                        <p>"A hotel room all to myself is my idea of a good time." ~ Chelsea Handler</p>
                    </div>
                </div>
            </div>




			<div class="tz-gallery">
				<div class="row">
                <?php
                $result = $connectDB -> query("SELECT * FROM category" );

                $count = 1;
                while($row = mysqli_fetch_array($result))
                {
                    $category = $row['category'];
                    ?>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <a class="lightbox" href="admin/img/<?php echo  $row['image'];?>">
                            <img class="img-fluid" src="admin/img/<?php echo  $row['image'];?>"  style="height:350px;" alt="Gallery Images">
                        </a>
                    </div>
                <?php }?>
				</div>
			</div>
		</div>
	</div>
	<!-- End Gallery -->
	
	<!-- Start Customer Reviews -->
<!--	<div class="customer-reviews-box">-->
<!--		<div class="container">-->
<!--			<div class="row">-->
<!--				<div class="col-lg-12">-->
<!--					<div class="heading-title text-center">-->
<!--						<h2>Customer Reviews</h2>-->
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--			<div class="row">-->
<!--				<div class="col-md-8 mr-auto ml-auto text-center">-->
<!--					<div id="reviews" class="carousel slide" data-ride="carousel">-->
<!--						<div class="carousel-inner mt-4">-->
<!--							<div class="carousel-item text-center active">-->
<!--								<div class="img-box p-1 border rounded-circle m-auto">-->
<!--									<img class="d-block w-100 rounded-circle" src="images/profile-1.jpg" alt="">-->
<!--								</div>-->
<!--								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">John Doe</strong></h5>-->
<!--								<p class="m-0 pt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>-->
<!--							</div>-->
<!--							<div class="carousel-item text-center">-->
<!--								<div class="img-box p-1 border rounded-circle m-auto">-->
<!--									<img class="d-block w-100 rounded-circle" src="images/profile-3.jpg" alt="">-->
<!--								</div>-->
<!--								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Jane Doe</strong></h5>-->
<!--								<p class="m-0 pt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>-->
<!--							</div>-->
<!--							<div class="carousel-item text-center">-->
<!--								<div class="img-box p-1 border rounded-circle m-auto">-->
<!--									<img class="d-block w-100 rounded-circle" src="images/profile-7.jpg" alt="">-->
<!--								</div>-->
<!--								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Daniel vebar</strong></h5>-->
<!--								<p class="m-0 pt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>-->
<!--							</div>-->
<!--						</div>-->
<!--						<a class="carousel-control-prev" href="#reviews" role="button" data-slide="prev">-->
<!--							<i class="fa fa-angle-left" aria-hidden="true"></i>-->
<!--							<span class="sr-only">Previous</span>-->
<!--						</a>-->
<!--						<a class="carousel-control-next" href="#reviews" role="button" data-slide="next">-->
<!--							<i class="fa fa-angle-right" aria-hidden="true"></i>-->
<!--							<span class="sr-only">Next</span>-->
<!--						</a>-->
<!--                    </div>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
	<!-- End Customer Reviews -->
	
	<!-- Start Contact info -->
	<?php include "footer.php";?>