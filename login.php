<?php 
 $msg='';

 // 

 include 'connection.php';
if(isset($_POST['submit'])){
    if( $_SERVER['REQUEST_METHOD']=='POST' && isset( $_POST['submit'], $_POST['username'], $_POST['password'] ) ) {
        if( empty( $_POST['username'] ) || empty( $_POST['password'] ) ){
            $msg = 'Both fields are required.';
        }else {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $result = $connectDB -> query("SELECT * FROM user where username ='$username' and password ='$password'" );
            // Perform query
            if ($result ->num_rows != 0) {
            	
            	$rows = $result->fetch_array(MYSQLI_BOTH);
				session_start(); 

            	$_SESSION['id'] = $rows['user_id'];
            	$_SESSION['name'] = $rows['fname']." ".$rows['lname'];
                $_SESSION['msg']='';

            	header("Location: index.php");

              
               // $msg =    mysqli_free_result($result) ;
            }else{
                $msg='Incorrect username or password.';
            }

//            if( isset( $_SESSION['username'] ) ) exit( header( 'location: index.php' ) );

        }
    }
}
?>
<?php include 'header.php';?>
	<!-- Start All Pages -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Login</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->
	
	<!-- Start Contact -->
	<div class="contact-box">
		<div class="container">
			<div id="msgSubmit" class="h3 text-center" style="color:green;"> <?php  echo $msg ;?></div><br>
			
			<div class="row">
				<div class="col-lg-12">
					<form  method="POST">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control" id="name" name="username" placeholder="Your username" required data-error="Please enter your username">
									<div class="help-block with-errors"></div>
								</div>                                 
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<input type="password" placeholder="Your password" id="password" class="form-control" name="password" required data-error="Please enter your password">
									<div class="help-block with-errors"></div>
								</div> 
							</div>
						
							<div class="col-md-12">
								
								<div class="submit-button text-center">
									<button class="btn btn-common" id="submit" type="submit" name="submit" value="submit">Login</button>
									<div id="msgSubmit" class="h3 text-center hidden"></div> 
									<div class="clearfix"></div> 
								</div>
							</div>
							<a href="signUp.php">Create user account?</a>
						</div>            
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- End Contact -->
	
	<!-- Start Contact info -->
<div class="contact-imfo-box">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<i class="fa fa-volume-control-phone"></i>
					<div class="overflow-hidden">
						<h4>Phone</h4>
						<p class="lead">
							09094728931
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-envelope"></i>
					<div class="overflow-hidden">
						<h4>Email</h4>
						<p class="lead">
							costaleonahotel@gmail.com
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-map-marker"></i>
					<div class="overflow-hidden">
						<h4>Location</h4>
						<p class="lead">
							Costa Leona, Nabas, Aklan
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Contact info -->
	
	<!-- Start Footer -->
	<?php include "footer.php";?>