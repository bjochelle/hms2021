<?php include 'header.php';?>
	
	<!-- Start header -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>FAQ</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End header -->
	
	<!-- Start About -->
	<div class="about-section-box">
		<div class="container">
			<div class="row">
				
				<div class="col-lg-12 Col-sm-12">
					<div class="inner-column">

						<h1><span>USER ACCOUNT</span></h1>
						<p><strong>HOME</strong> - The welcome page for the client when they browse the system.</p>
						<p><strong>GALLERY</strong> - The page where the photos of the hotel can be viewed.</p>
						<p><strong>ROOMS</strong> - The page where the details of the room display which is available accordingly to the selected dates of the clients also where they can book their desired room.
                            .</p>
						<p><strong>ABOUT US</strong> - The page where clients can leave comments, suggestions or questions.</p>

                        <h1><span>ADMIN ACCOUNT</span></h1>
						<p><strong>CUSTOMER</strong> - The page where the admin can see the list of customer.</p>
						<p><strong>TRANSACTIONS</strong> - The page where the admin can confirm the clients bookings.</p>
						<p><strong>ROOMS</strong> - The page where can admin managed the availability of rooms of the hotel such as Add, Update, Delete rooms availability.</p>
                        <p><strong>CATEGORY</strong> - The page where can admin manage the list of room categories such as Add, Update, Delete room category.</p>
                        <p><strong>MAINTENANCE</strong> - The page where can admin add photos in the gallery.</p>
                        <p><strong>SETTINGS</strong> - The page where can admin configures the hotel name, tagline and the about content.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End About -->
	
	
	<!-- Start Contact info -->
	<?php include "footer.php";?>