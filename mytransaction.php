<?php include 'header.php';?>

    <!-- Start All Pages -->
    <div class="all-page-title page-breadcrumb">
        <div class="container text-center">
            <div class="row">
                <div class="col-lg-12">
                    <h1>My Transaction  <br><?php echo $_SESSION['msg'];?></h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Pages -->

    <!-- Start Menu -->
    <div class="menu-box">
        <div class="container">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Room #</th>
                            <th>Category</th>
                            <th>Check In</th>
                            <th>Check Out</th>
                            <th>Status</th>
                            <th>Option</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $result = $connectDB -> query("SELECT * FROM  reservation" );

                        while($row = mysqli_fetch_array($result))
                        {
                            $resultRoom = $connectDB -> query("SELECT * FROM  category as c, rooms as r where c.category=r.category and room_id='$row[room_id]'" );
                            $rowRoom = mysqli_fetch_array($resultRoom);

                            $resultCustomer = $connectDB -> query("SELECT * FROM  user where user_id='$row[user_id]'" );
                            $rowCustomer = mysqli_fetch_array($resultCustomer);

                            if($row['status'] === 'P'){
                                $status = '<span class="alert alert-info">Pending</span>';
                            }elseif ($row['status'] === 'C'){
                                $status = '<span class="alert alert-primary">Confirmed</span>';
                            }elseif ($row['status'] === 'X'){
                                $status = '<span class="alert alert-danger">Cancelled</span>';
                            }else{
                                $status = '<span class="alert alert-success">Finished</span>';
                            }

                            ?>
                            <tr>
                                <td><?php echo  $rowRoom['room_number'];?></td>
                                <td><?php echo  $rowRoom['category'];?></td>
                                <td><?php echo  $row['check_in'];?></td>
                                <td><?php echo  $row['check_out'];?></td>
                                <td class="text-center"><?php echo $status;?></td>
                                <td>
                                    <a href="#"  class="btn btn-primary" data-toggle="modal" data-target="#viewModal<?php echo  $row['id'];?>"><i class="fa fa-eye"></i>
                                        View</a>

                                    <div class="modal fade" id="viewModal<?php echo  $row['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">View Details</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Room # : </label>
                                                        <span> <?php echo  $rowRoom['room_number'];?> </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Category : </label>
                                                        <span> <?php echo  $rowRoom['category'];?> </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Customer : </label>
                                                        <span> <?php echo   $rowCustomer['fname']." ".$rowCustomer['lname'];?> </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Check In : </label>
                                                        <span> <?php echo  $row['check_in'];?> </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Check Out : </label>
                                                        <span> <?php echo  $row['check_out'];?> </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Number of Days : </label>
                                                        <span> <?php echo  $row['num_days'];?> </span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Total : </label>
                                                        <span> Php <?php echo  $row['total'];?> </span>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>

                        <?php }

                        ?>

                        </tbody>
                    </table>
                </div>


        </div>
    </div>
    <!-- End Menu -->
    <!-- Start Contact info -->
<?php include "footer.php";?>