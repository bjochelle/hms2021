<?php include 'header.php';?>

    <!-- Start All Pages -->
    <div class="all-page-title page-breadcrumb">
        <div class="container text-center">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Reservation  <br><?php echo $_SESSION['msg'];?></h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Pages -->

    <!-- Start Menu -->
    <div class="menu-box">
        <div class="container">


            <div class="row special-list">
                <?php
                $result = $connectDB -> query("SELECT * FROM  category as c, rooms as r where c.category=r.category and room_id='$_GET[id]'" );
                $row = mysqli_fetch_array($result);
               ?>
                <div class="col-lg-12 special-grid drinks">
                    <div class="col-lg-6 gallery-single fix" style="float: left">
                        <img src="admin/img/<?php echo  $row['image'];?>" class="img-fluid" alt="Image" style="object-fit: cover;height: 300px;width: 100%;">
                    </div>
                    <div class="col-lg-6" style="float: left;top: 80px;">
                        <h1><?php echo  $row['description'];?></h1>
                        <h3>Php <?php echo  $row['price'];?></h3>
                        <h1>Room number: <?php echo  $row['room_number'];?></h1>  <br>  <br><br>
                        <br>
                    </div
                </div>

                <form method="POST" action="php/addReservation.php">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Check In</label>
                            <div class="form-group">
                                <input type="date" class="form-control" id="address" name="check_in" required />
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label>Check Out</label>
                            <div class="form-group">
                                <input type="date" class="form-control" id="address" name="check_out" required />
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <input type="hidden" class="form-control" name="user_id" value="<?php echo $_SESSION['id'];?>" />
                        <input type="hidden" class="form-control"  name="room_id" value="<?php echo $_GET['id'];?>" />
                        <input type="hidden" class="form-control"  name="price" value="<?php echo $row['price'];?>" />


                        <div class="submit-button text-center">
                            <button class="btn btn-common" id="submit" name="submit" type="submit"  value="Submit" >Reserve now</button>

                            <div class="clearfix"></div>
                        </div>
                    </div>
            </div>
            </form>

            </div>
        </div>
    </div>
    <!-- End Menu -->
    <!-- Start Contact info -->
<?php include "footer.php";?>