<?php include 'header.php';?>
	
	<!-- Start All Pages -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Gallery</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->
	
	<!-- Start Gallery -->
	<div class="gallery-box">
		<div class="container-fluid">
		
			<div class="tz-gallery">
				<div class="row">
                <?php
                $result = $connectDB -> query("SELECT * FROM gallery" );

                $count = 1;
                if($result){
                while($row = mysqli_fetch_array($result))
                {?>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <a class="lightbox" href="admin/img/gallery/<?php echo  $row['image'];?>"">
                            <img class="img-fluid" src="admin/img/gallery/<?php echo  $row['image'];?>" classalt="Gallery Images" style="height: 400px;">
                        </a>
                    </div>

                <?php }}?>
				</div>
			</div>
		</div>
	</div>
	<!-- End Gallery -->
	
	
	<!-- Start Contact info -->
	<?php include "footer.php";?>