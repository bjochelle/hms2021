<?php include "header.php"; ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Category</h1>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List of Category</h6>
            </div>
            <div class="card-body">
                <div class="">
                    <a href="#" class="btn btn-primary" style="float: right" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i>
                        Add Category</a>
                </div><br><br>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="php/addCategory.php" method="POST" enctype="multipart/form-data">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Image</label>
                                    <input type="file" class="form-control" id="capacity" placeholder="Image" name="image">
                                </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Category</label>
                                        <select class="form-control" id="exampleFormControlSelect1" name="category">
                                            <option>Standard</option>
                                            <option>Deluxe</option>
                                            <option>Presedential</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Capacity</label>
                                        <input type="number" min="0" step="1" class="form-control"  name="capacity" id="capacity" placeholder="Capacity">
                                    </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Price</label>
                                    <input type="number" min="0" step="0.01" class="form-control" name="price" id="price" placeholder="Price">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Description</label>
                                    <textarea type="text"  class="form-control" id="Description" name="description" placeholder="Description"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" name="upload">Submit</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Image</th>
                            <th>Category</th>
                            <th>Capacity</th>
                            <th>Price</th>
                            <th>Description</th>
                            <th>Option</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                         $result = $connectDB -> query("SELECT * FROM category" );

                         $count = 1;
                         while($row = mysqli_fetch_array($result))
                        {
                            $category = $row['category'];

                         ?>
                          <tr>
                             <td><?php echo  $count++;?></td>
                            <td><center><div style="object-fit: cover;width: 50%;"><img src="img/<?php echo  $row['image'];?>" class="img-fluid" alt="Image"style="height: 100px;object-fit: cover;"></div></center></td>
                             <td><?php echo  $row['category'];?></td>
                             <td><?php echo  $row['capacity'];?></td>
                             <td><?php echo  $row['price'];?></td>
                             <td><?php echo  $row['description'];?></td>
                             <td> <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#editModal<?php echo  $row['id'];?>"><i class="fa fa-edit"></i>
                                    Update</a>
                                    <div class="modal fade" id="editModal<?php echo  $row['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                       
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Update Category</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form action="php/updateCategory.php" method="POST" enctype="multipart/form-data">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Image</label>
                                                        <input type="file" class="form-control" id="capacity" placeholder="Image" name="image">
                                                    </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Category</label>
                                                            <select class="form-control" id="category" name="category">
                                                                <option value="standard"<?php  if($category === 'standard'){ echo 'selected';} ?> > Standard</option>
                                                                <option value="deluxe"  <?php  if($category === 'deluxe'){ echo 'selected';}  ?> >Deluxe</option>
                                                                <option value="presedential"  <?php   if($category === 'presedential'){ echo 'selected';}  ?>>Presedential</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1">Capacity</label>
                                                            <input type="number" min="0" step="1" class="form-control"  name="capacity" id="capacity" placeholder="Capacity" value="<?php echo $row['capacity'];?>">
                                                        </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Price</label>
                                                        <input type="number" min="0" step="0.01" class="form-control" name="price" id="price" placeholder="Price"  value="<?php echo $row['price'];?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Description</label>
                                                        <textarea type="text"  class="form-control" id="Description" name="description" placeholder="Description"><?php echo  $row['description'];?></textarea>
                                                    </div>
                                                </div>
                                                    <input type="hidden" name="id"  value="<?php echo $row['id'];?>">

                                                    <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary" name="update">Submit</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                <a href="php/deleteCategory.php?id=<?php echo $row['id'];?>" class="btn btn-danger"><i class="fa fa-trash"></i>
                                    Delete</a></td>
                         </tr> 
                        <?php }

                        ?>
                      
                        </tbody>
                    </table>
                    <!-- Modal -->
                    
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php include "footer.php"; ?>