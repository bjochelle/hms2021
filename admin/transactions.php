<?php include "header.php"; ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Transaction Table</h1>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List of Transaction</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Room #</th>
                            <th>Category</th>
                            <th>Customer</th>
                            <th>Check In</th>
                            <th>Check Out</th>
                            <th>Status</th>
                            <th>Option</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $result = $connectDB -> query("SELECT * FROM  reservation" );

                        while($row = mysqli_fetch_array($result))
                        {
                            $resultRoom = $connectDB -> query("SELECT * FROM  category as c, rooms as r where c.category=r.category and room_id='$row[room_id]'" );
                            $rowRoom = mysqli_fetch_array($resultRoom);

                            $resultCustomer = $connectDB -> query("SELECT * FROM  user where user_id='$row[user_id]'" );
                            $rowCustomer = mysqli_fetch_array($resultCustomer);

                            if($row['status'] === 'P'){
                                $status = '<span class="alert alert-info">Pending</span>';
                                $confirmed = '<a href="php/updateRes.php?id='.$row['id'].'&status=C"  class="btn btn-info"><i class="fa fa-check"></i>
                                        Confirmed</a>';
                                $checkout ='';
                            }elseif ($row['status'] === 'C'){
                                $confirmed ='';
                                $checkout = '<a href="php/updateRes.php?id='.$row['id'].'&status=F" class="btn btn-warning"><i class="fa fa-check"></i>
                                Check Out</a>';
                                $status = '<span class="alert alert-primary">Confirmed</span>';
                            }elseif ($row['status'] === 'X'){
                                $confirmed ='';
                                $checkout ='';
                                $status = '<span class="alert alert-danger">Cancelled</span>';
                            }else{
                                $confirmed ='';
                                $checkout ='';
                                $status = '<span class="alert alert-success">Finished</span>';
                            }

                            ?>
                            <tr>
                                <td><?php echo  $rowRoom['room_number'];?></td>
                                <td><?php echo  $rowRoom['category'];?></td>
                                <td><?php echo  $rowCustomer['fname']." ".$rowCustomer['lname'];?></td>
                                <td><?php echo  $row['check_in'];?></td>
                                <td><?php echo  $row['check_out'];?></td>
                                <td class="text-center"><?php echo $status;?></td>
                                <td>
                                    <?php echo $confirmed;?>
                                    <?php echo $checkout;?>
                                    <a href="#"  class="btn btn-primary" data-toggle="modal" data-target="#viewModal<?php echo  $row['id'];?>"><i class="fa fa-eye"></i>
                                        View</a>

                                    <div class="modal fade" id="viewModal<?php echo  $row['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">View Details</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1">Room # : </label>
                                                            <span> <?php echo  $rowRoom['room_number'];?> </span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Category : </label>
                                                            <span> <?php echo  $rowRoom['category'];?> </span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Customer : </label>
                                                            <span> <?php echo   $rowCustomer['fname']." ".$rowCustomer['lname'];?> </span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Check In : </label>
                                                            <span> <?php echo  $row['check_in'];?> </span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Check Out : </label>
                                                            <span> <?php echo  $row['check_out'];?> </span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Number of Days : </label>
                                                            <span> <?php echo  $row['num_days'];?> </span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Total : </label>
                                                            <span> Php <?php echo  $row['total'];?> </span>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>

                        <?php }

                        ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php include "footer.php"; ?>