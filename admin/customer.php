<?php include "header.php"; ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Customer Table</h1>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List of Customer</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Contact</th>
                        </tr>
                        </thead>
                       <tbody>

                        <?php 
                         $result = $connectDB -> query("SELECT * FROM user where user_type='C'" );

                         while($row = mysqli_fetch_array($result))
                        { ?>
                          <tr>
                             <td><?php echo  $row['fname']." ".$row['lname'];?></td>
                             <td><?php echo  $row['address'];?></td>
                             <td><?php echo  $row['email'];?></td>
                             <td><?php echo  $row['contact_number'];?></td>
                         </tr> 
                        <?php }

                        ?>
                      
                       </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php include "footer.php"; ?>