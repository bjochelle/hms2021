<?php include "header.php"; ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Settings</h1>
        </div>

        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12 col-md-12 mb-4">
                <div class="card shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <form class="col-xl-12">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Hotel Name</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput1" value="La Costa Leona" placeholder="Hotel Name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Mobile Number</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput1" value="09094728931" placeholder="Mobile Number">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Email</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput1" value="costaleonahotel@gmail.com" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Address</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">Costa Leona, Nabas, Aklan</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">About Us</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">Costa Leona Hotel is a newest modern hotel. We aim to provide peace of mind, consistency and build loyalty based on our value of our relationships with the clients.</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Tagline</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput1" value="Above All Elegance" placeholder="Tagline">
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- Content Row -->


    </div>
    <!-- /.container-fluid -->
<?php include "footer.php"; ?>