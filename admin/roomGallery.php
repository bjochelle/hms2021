<?php include "header.php"; ?>

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Gallery</h1>
        </div>

        <!-- Content Row -->
        <div class="row">

            <div class="col-xl-12">
                <a href="#" class="btn btn-primary" style="float: right" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i>
                    Add Gallery</a>
            </div><br><br>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Gallery</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form  action="php/addGallery.php" method="POST" enctype="multipart/form-data">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Image</label>
                                    <input type="file" class="form-control" id="capacity" placeholder="Image" name="image">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" name="upload">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <br><br>

            <!-- Earnings (Monthly) Card Example -->
            <?php
            $result = $connectDB -> query("SELECT * FROM gallery" );

            $count = 1;
            if($result){
            while($row = mysqli_fetch_array($result))
            {?>

                <div class="col-xl-4 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <a  href="php/deleteGallery.php?id=<?php echo $row['id'];?>" ><span class="fa fa-trash" style="float: right;color: #e74a3b;"></span> </a><br>
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <img  src="img/gallery/<?php echo  $row['image'];?>" class="img-fluid img-galery" alt="Image" style="height: 300px;width:100%;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }}?>


        </div>

        <!-- Content Row -->
    </div>
    <!-- /.container-fluid -->
    <style>
        .img-galery{
            height: 300px;
        }
    </style>
<?php include "footer.php"; ?>