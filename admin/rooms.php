<?php include "header.php"; ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Rooms</h1>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List of Room</h6>
            </div>
            <div class="card-body">
                <div class="">
                    <a href="#" class="btn btn-primary" style="float: right" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i>
                        Add Room</a>
                </div><br><br>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add Room</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="php/addRoom.php" method="POST">
                                <div class="modal-body">

                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Room #</label>
                                        <input type="text" class="form-control" id="capacity" placeholder="Room #" name="room_number">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Category</label>
                                        <select class="form-control" id="exampleFormControlSelect1" name="category">
                                            <option>Standard</option>
                                            <option>Deluxe</option>
                                            <option>Presedential</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Room #</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th>Option</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $result = $connectDB -> query("SELECT * FROM rooms " );
                        $count = 1;
                        while($row = mysqli_fetch_array($result))
                        {
                            if($row['status'] === 'A'){
                                $staus = '<span class="alert alert-success">Available</span>';
                            }else{
                                $staus = '<span class="alert alert-warning">Not Available</span>';
                            }

                            $category = $row['category'];

                            ?>
                            <tr>
                                <td><?php echo  $count++;?></td>
                                <td><?php echo  $row['room_number'];?></td>
                                <td><?php echo  $category;?></td>
                                <td class="text-center"><?php echo  $staus;?></td>
                                <td>
                                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#editModal<?php echo $row['room_id'];?>"><i class="fa fa-edit"></i>
                                        Update</a>

                                    <div class="modal fade" id="editModal<?php echo $row['room_id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit Rooms</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form action="php/updateRoom.php" method="POST">
                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label for="exampleInputPassword1">Room #</label>
                                                            <input type="text" class="form-control" id="capacity" placeholder="Room #" name="room_number" value="<?php echo $row['room_number'];?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Category</label>
                                                                <select class="form-control" id="category" name="category">
                                                                    <option value="Standard"<?php  if($category === 'Standard'){ echo 'selected';} ?> > Standard</option>
                                                                    <option value="Deluxe"  <?php  if($category === 'Deluxe'){ echo 'selected';}  ?> >Deluxe</option>
                                                                    <option value="Presedential"  <?php   if($category === 'Presedential'){ echo 'selected';}  ?>>Presedential</option>
                                                                </select>
                                                        </div>

                                                    </div>
                                                    <input type="hidden" name="id"  value="<?php echo $row['room_id'];?>">
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <a href="php/deleteRoom.php?id=<?php echo $row['room_id'];?>" class="btn btn-danger"><i class="fa fa-trash"></i>
                                        Delete</a>
                                </td>
                            </tr>
                        <?php }
                        ?>


                        </tbody>
                    </table>
                    <!-- Modal -->
                    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form>
                                    <div class="modal-body">

                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Room #</label>
                                            <input type="text" class="form-control" id="capacity" placeholder="Room #">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Category</label>
                                            <select class="form-control" id="exampleFormControlSelect1">
                                                <option>Standard</option>
                                                <option>Deluxe</option>
                                                <option>Presedential</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php include "footer.php"; ?>